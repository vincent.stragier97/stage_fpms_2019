EESchema Schematic File Version 4
LIBS:shield_moteur-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_Motor:L293D U1
U 1 1 5C83C137
P 7900 3450
F 0 "U1" H 7900 4628 50  0000 C CNN
F 1 "L293D" H 7900 4537 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 8150 2700 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/l293.pdf" H 7600 4150 50  0001 C CNN
	1    7900 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5C83C2B0
P 7700 4350
F 0 "#PWR0101" H 7700 4100 50  0001 C CNN
F 1 "GND" H 7705 4177 50  0000 C CNN
F 2 "" H 7700 4350 50  0001 C CNN
F 3 "" H 7700 4350 50  0001 C CNN
	1    7700 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 4250 7700 4300
Wire Wire Line
	7700 4300 7800 4300
Wire Wire Line
	8100 4300 8100 4250
Connection ~ 7700 4300
Wire Wire Line
	7700 4300 7700 4350
Wire Wire Line
	8000 4250 8000 4300
Connection ~ 8000 4300
Wire Wire Line
	8000 4300 8100 4300
Wire Wire Line
	7800 4250 7800 4300
Connection ~ 7800 4300
Wire Wire Line
	7800 4300 8000 4300
$Comp
L arduino_shieldsNCL:ARDUINO_SHIELD SHIELD1
U 1 1 5C83C379
P 3950 3450
F 0 "SHIELD1" H 3950 4537 60  0000 C CNN
F 1 "ARDUINO_SHIELD" H 3950 4431 60  0000 C CNN
F 2 "arduino_shields:ARDUINO SHIELD" H 3950 3450 50  0001 C CNN
F 3 "" H 3950 3450 50  0001 C CNN
	1    3950 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5C83C729
P 5800 2750
F 0 "#PWR0102" H 5800 2500 50  0001 C CNN
F 1 "GND" H 5805 2577 50  0000 C CNN
F 2 "" H 5800 2750 50  0001 C CNN
F 3 "" H 5800 2750 50  0001 C CNN
	1    5800 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J5
U 1 1 5C83C9AE
P 5100 3950
F 0 "J5" H 4993 3325 50  0000 C CNN
F 1 "Conn_01x08_Female" H 4993 3416 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 5100 3950 50  0001 C CNN
F 3 "~" H 5100 3950 50  0001 C CNN
	1    5100 3950
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x06_Female J3
U 1 1 5C83CA8F
P 2800 3950
F 0 "J3" H 2694 4335 50  0000 C CNN
F 1 "Conn_01x06_Female" H 3250 3950 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 2800 3950 50  0001 C CNN
F 3 "~" H 2800 3950 50  0001 C CNN
	1    2800 3950
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J6
U 1 1 5C83D0FA
P 5550 3050
F 0 "J6" H 5443 2425 50  0000 C CNN
F 1 "Conn_01x08_Female" H 5443 2516 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x08_Pitch2.54mm" H 5550 3050 50  0001 C CNN
F 3 "~" H 5550 3050 50  0001 C CNN
	1    5550 3050
	1    0    0    1   
$EndComp
Wire Wire Line
	4900 2650 5350 2650
Wire Wire Line
	4900 3350 5350 3350
Wire Wire Line
	4900 3250 5350 3250
Wire Wire Line
	4900 3150 5350 3150
Wire Wire Line
	5350 3050 4900 3050
Wire Wire Line
	4900 2950 5350 2950
Wire Wire Line
	4900 2750 5300 2750
Wire Wire Line
	5300 2750 5300 2700
Wire Wire Line
	5300 2700 5800 2700
Wire Wire Line
	5800 2700 5800 2750
Connection ~ 5300 2750
Wire Wire Line
	5300 2750 5350 2750
Wire Wire Line
	4900 2850 5350 2850
$Comp
L Connector:Conn_01x06_Female J2
U 1 1 5C83F203
P 2600 3250
F 0 "J2" H 2494 3635 50  0000 C CNN
F 1 "Conn_01x06_Female" H 3050 3350 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 2600 3250 50  0001 C CNN
F 3 "~" H 2600 3250 50  0001 C CNN
	1    2600 3250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3000 3550 2900 3550
Wire Wire Line
	3000 3450 2900 3450
Wire Wire Line
	3000 3350 2900 3350
Wire Wire Line
	2800 3250 2900 3250
Wire Wire Line
	3000 3150 2800 3150
Wire Wire Line
	2800 3050 3000 3050
Wire Wire Line
	2900 3350 2900 3400
Connection ~ 2900 3350
Wire Wire Line
	2900 3350 2800 3350
Connection ~ 2900 3450
Wire Wire Line
	2900 3450 2800 3450
Wire Wire Line
	2900 3400 2450 3400
Wire Wire Line
	2450 3400 2450 3500
Connection ~ 2900 3400
Wire Wire Line
	2900 3400 2900 3450
$Comp
L power:GND #PWR0103
U 1 1 5C8412B5
P 2450 3500
F 0 "#PWR0103" H 2450 3250 50  0001 C CNN
F 1 "GND" H 2455 3327 50  0000 C CNN
F 2 "" H 2450 3500 50  0001 C CNN
F 3 "" H 2450 3500 50  0001 C CNN
	1    2450 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J7
U 1 1 5C843ADF
P 9650 2650
F 0 "J7" H 9677 2676 50  0000 L CNN
F 1 "Conn_01x03_Female" H 9677 2585 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x03_Pitch2.54mm" H 9650 2650 50  0001 C CNN
F 3 "~" H 9650 2650 50  0001 C CNN
	1    9650 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J9
U 1 1 5C843B51
P 9650 3500
F 0 "J9" H 9677 3526 50  0000 L CNN
F 1 "Conn_01x03_Female" H 9677 3435 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x03_Pitch2.54mm" H 9650 3500 50  0001 C CNN
F 3 "~" H 9650 3500 50  0001 C CNN
	1    9650 3500
	1    0    0    -1  
$EndComp
Text GLabel 2800 3650 0    50   Input ~ 0
VIN
Wire Wire Line
	2800 3650 2900 3650
Wire Wire Line
	2900 3650 2900 3550
Connection ~ 2900 3550
Wire Wire Line
	2900 3550 2800 3550
Wire Wire Line
	8000 2450 8000 2350
Wire Wire Line
	8000 2350 8150 2350
Text GLabel 8150 2350 2    50   Input ~ 0
VIN
$Comp
L Connector:Conn_01x02_Female J8
U 1 1 5C84593F
P 9650 3000
F 0 "J8" H 9677 2976 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9677 2885 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x02_Pitch2.54mm" H 9650 3000 50  0001 C CNN
F 3 "~" H 9650 3000 50  0001 C CNN
	1    9650 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J10
U 1 1 5C84597F
P 9650 3850
F 0 "J10" H 9677 3826 50  0000 L CNN
F 1 "Conn_01x02_Female" H 9677 3735 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x02_Pitch2.54mm" H 9650 3850 50  0001 C CNN
F 3 "~" H 9650 3850 50  0001 C CNN
	1    9650 3850
	1    0    0    -1  
$EndComp
Text GLabel 2500 3300 0    50   Input ~ 0
5V
Wire Wire Line
	2500 3300 2900 3300
Wire Wire Line
	2900 3300 2900 3250
Connection ~ 2900 3250
Wire Wire Line
	2900 3250 3000 3250
Text GLabel 7650 2350 0    50   Input ~ 0
5V
Wire Wire Line
	7650 2350 7800 2350
Wire Wire Line
	7800 2350 7800 2450
Text GLabel 7400 3250 0    50   Input ~ 0
1
Text GLabel 7400 3050 0    50   Input ~ 0
7
Text GLabel 7400 3450 0    50   Input ~ 0
10
Text GLabel 7400 3650 0    50   Input ~ 0
15
Text GLabel 7400 3850 0    50   Input ~ 0
9
Text GLabel 7400 2850 0    50   Input ~ 0
2
Text GLabel 8400 2850 2    50   Input ~ 0
3
Text GLabel 8400 3050 2    50   Input ~ 0
6
Text GLabel 8400 3450 2    50   Input ~ 0
11
Text GLabel 8400 3650 2    50   Input ~ 0
14
Text GLabel 9350 2650 0    50   Input ~ 0
2
Text GLabel 9350 2550 0    50   Input ~ 0
1
Text GLabel 9350 2750 0    50   Input ~ 0
3
Text GLabel 9350 3000 0    50   Input ~ 0
6
Text GLabel 9350 3100 0    50   Input ~ 0
7
Text GLabel 9350 3400 0    50   Input ~ 0
9
Text GLabel 9350 3500 0    50   Input ~ 0
10
Text GLabel 9350 3600 0    50   Input ~ 0
11
Text GLabel 9350 3850 0    50   Input ~ 0
14
Text GLabel 9350 3950 0    50   Input ~ 0
15
Wire Wire Line
	9450 2550 9350 2550
Wire Wire Line
	9450 2650 9350 2650
Wire Wire Line
	9450 2750 9350 2750
Wire Wire Line
	9450 3000 9350 3000
Wire Wire Line
	9450 3100 9350 3100
Wire Wire Line
	9450 3400 9350 3400
Wire Wire Line
	9450 3500 9350 3500
Wire Wire Line
	9450 3600 9350 3600
Wire Wire Line
	9350 3850 9450 3850
Wire Wire Line
	9450 3950 9350 3950
Wire Notes Line
	6850 1800 10750 1800
Wire Notes Line
	10750 1800 10750 4700
Wire Notes Line
	10750 4700 6850 4700
Wire Notes Line
	6850 1800 6850 4700
Wire Notes Line
	6100 1800 1500 1800
Wire Notes Line
	6100 1800 6100 4700
Wire Notes Line
	1500 1800 1500 4700
Wire Notes Line
	1500 4700 6100 4700
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 5C842BA8
P 2300 2200
F 0 "J1" H 2327 2176 50  0000 L CNN
F 1 "Conn_01x04_Female" H 2327 2085 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 2300 2200 50  0001 C CNN
F 3 "~" H 2300 2200 50  0001 C CNN
	1    2300 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 5C842BE4
P 2900 2200
F 0 "J4" H 2927 2176 50  0000 L CNN
F 1 "Conn_01x04_Female" H 2927 2085 50  0000 L CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x04_Pitch2.54mm" H 2900 2200 50  0001 C CNN
F 3 "~" H 2900 2200 50  0001 C CNN
	1    2900 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2200 2650 2200
Wire Wire Line
	2650 2200 2650 2250
Wire Wire Line
	2650 2300 2700 2300
Wire Wire Line
	2100 2200 2050 2200
Wire Wire Line
	2050 2200 2050 2250
Wire Wire Line
	2050 2300 2100 2300
Wire Wire Line
	2100 2100 2050 2100
Wire Wire Line
	2050 2100 2050 2000
Wire Wire Line
	2050 2000 2700 2000
Wire Wire Line
	2700 2000 2700 2100
Wire Wire Line
	2100 2400 2050 2400
Wire Wire Line
	2050 2400 2050 2500
Wire Wire Line
	2050 2500 2700 2500
Wire Wire Line
	2700 2500 2700 2400
Wire Wire Line
	2700 2000 2700 1900
Wire Wire Line
	2700 1900 2900 1900
Connection ~ 2700 2000
$Comp
L power:GND #PWR0104
U 1 1 5C84C611
P 1900 2350
F 0 "#PWR0104" H 1900 2100 50  0001 C CNN
F 1 "GND" H 1905 2177 50  0000 C CNN
F 2 "" H 1900 2350 50  0001 C CNN
F 3 "" H 1900 2350 50  0001 C CNN
	1    1900 2350
	1    0    0    -1  
$EndComp
Text GLabel 2900 1900 2    50   Input ~ 0
5V
Text GLabel 2900 2600 2    50   Input ~ 0
VIN
Wire Wire Line
	1900 2250 2050 2250
Connection ~ 2650 2250
Wire Wire Line
	2650 2250 2650 2300
Wire Wire Line
	1900 2350 1900 2250
Connection ~ 2050 2250
Wire Wire Line
	2050 2250 2050 2300
Wire Wire Line
	2050 2250 2650 2250
Wire Wire Line
	2900 2600 2700 2600
Wire Wire Line
	2700 2600 2700 2500
Connection ~ 2700 2500
Text GLabel 5150 3800 2    50   Input ~ 0
10
Text GLabel 5150 3700 2    50   Input ~ 0
15
Text GLabel 5200 3900 2    50   Input ~ 0
9
Wire Wire Line
	4900 3950 4900 3900
Wire Wire Line
	4900 3900 5200 3900
Connection ~ 4900 3950
Wire Wire Line
	5150 3800 4900 3800
Wire Wire Line
	4900 3800 4900 3850
Connection ~ 4900 3850
Wire Wire Line
	5150 3700 4900 3700
Wire Wire Line
	4900 3700 4900 3750
Connection ~ 4900 3750
$EndSCHEMATC
